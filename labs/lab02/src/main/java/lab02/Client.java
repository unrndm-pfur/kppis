package lab02;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.HashMap;
import java.util.stream.Collectors;


public class Client {
    private String username;
    private boolean newUser;
    private Map<Item, Integer> cart = new HashMap<>();
    private Order order;
    private String coupon = "";

    Client(String username) {
        this.username = username;
        this.newUser = true;
    }

    Client(String username, boolean newUser) {
        this.username = username;
        this.newUser = newUser;
    }

    public void register() {
        this.newUser = false;
    }

    @Override
    public String toString() {
        return super.toString()+
            "\nusername: "+this.username+
            "\nnewUser: "+this.newUser+
            "\ncart: [\n"+
                cart.entrySet().stream().map(
                        entry -> ("\t"+entry.getValue()+": ("+entry.getKey().toString()+"),\n")
                ).collect(Collectors.joining())+"]"+
            "\norder: "+order;
    }

    void add2Cart(Item item, Integer amount) {
        if (cart.containsKey(item)) {
            cart.put(item, cart.get(item)+amount);
        } else {
            cart.put(item, amount);
        }
    }

    public Map<Item, Integer> getCart() {
        return cart;
    }

    public boolean isNew() {
        return newUser;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public Order getOrder() {
        return order;
    }

    public String getCoupon() {
        return coupon;
    }

    public boolean hasCoupon() {
        return coupon.length() > 0;
    }

    public void setCoupon(String coupon) {
        this.coupon = coupon;
    }

    public String getUsername() {
        return username;
    }
}
