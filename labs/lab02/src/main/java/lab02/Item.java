package lab02;

public class Item {
    private String name;
    private String description;
    private Integer price;

    public Item(String name, String description, Integer price) {
        this.name = name;
        this.description = description;
        this.price = price;
    }

    public Integer getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return super.toString()+
            " name: "+this.name+
            " description: "+ (this.description.length() > 0 ? this.description : "NoDescription")+
            " price: "+this.price;
    }
}
