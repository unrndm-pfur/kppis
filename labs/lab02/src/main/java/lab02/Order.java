package lab02;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class Order {
    private Map<Item, Integer> items;
    private double discount = 0.0;
    private String status = "Not processed";

    public Order(Map<Item, Integer> items) {
        this.items = items;
    }

    @Override
    public String toString() {
        return super.toString()+
            "\nitems: [\n"+
            items.entrySet().stream().map(
                    entry -> ("\t"+entry.getValue()+": ("+entry.getKey().toString()+"),\n")
            ).collect(Collectors.joining())+"]"+
            "\ndiscount: "+discount+
            "\nprice: "+getPrice();
    }

    public Map<Item, Integer> getItems() {
        return items;
    }

    public double getPrice() {
        return items.entrySet().stream().reduce(
                0.0,
                (total, entry) -> (total + ( entry.getValue() * entry.getKey().getPrice())),
                Double::sum
        )*discount;
    }

    public double getDiscount() {
        return discount;
    }

    public void applyDiscount(Double value) {
        this.discount += value;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
